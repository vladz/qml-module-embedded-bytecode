#include <QGuiApplication>

#include "plugin/mod.h"

int main(int argc, char *argv[])
{
    QGuiApplication a(argc, argv);
    QQmlApplicationEngine engine = loadApp();
    return a.exec();
}
