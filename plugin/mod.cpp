#include "mod.h"

QQmlApplicationEngine loadApp() {
    return QQmlApplicationEngine(QStringLiteral("qrc:/mod/qml/main.qml"));
}
