add_library(plugin STATIC)

qt_add_qml_module(plugin
    NO_PLUGIN
    URI mod
    QML_FILES
        qml/main.qml
)

target_sources(plugin PRIVATE
    mod.cpp
)

target_link_libraries(plugin PUBLIC
    Qt6::Qml
)
